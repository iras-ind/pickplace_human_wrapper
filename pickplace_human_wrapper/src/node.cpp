/*
Copyright (c) 2020, JRL-CARI CNR-STIIMA/UNIBS
Manuel Beschi manuel.beschi@unibs.it
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <ros/ros.h>
#include <shared_human_body_part/BodyList.h>
#include <geometry_msgs/PoseArray.h>
#include <subscription_notifier/subscription_notifier.h>
#include <Eigen/Dense>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>

typedef std::shared_ptr<ros_helper::SubscriptionNotifier<shared_human_body_part::BodyList>> subPtr;
class PickPlaceWrapper
{
public:
  PickPlaceWrapper(ros::NodeHandle& nh);
  void publishPoseArray();
protected:
  ros::NodeHandle nh_;
  ros::Publisher array_pub_;
  std::map<std::string,subPtr> subs_;
  std::map<std::string,std::vector<geometry_msgs::Pose>> poses_in_world_frame;
  std::map<std::string,Eigen::Affine3d, std::less<std::string>,
  Eigen::aligned_allocator<std::pair<const std::string, Eigen::Affine3d>>> T_world_cameras;
  double accuracy_threshold;
  std::string robot_reference_frame;

  void bodeMsgCb(const shared_human_body_part::BodyListConstPtr& msg, const std::string& camera_frame);
  void convertBodyList(const shared_human_body_part::BodyListConstPtr& msg, std::vector<geometry_msgs::Pose> &poses_in_camera);
  void convertBody(const shared_human_body_part::Body& msg, std::vector<geometry_msgs::Pose> &poses_in_camera);
  geometry_msgs::Pose convertCoordinate3d(const shared_human_body_part::Coordinate3D& p);
};

PickPlaceWrapper::PickPlaceWrapper(ros::NodeHandle& nh)
{
  nh_=nh;

  std::string array_topic;
  if (!nh_.getParam("pose_array_topic",array_topic))
  {
    ROS_ERROR("%s/pose_array_topic is not defined, aborting",nh_.getNamespace().c_str());
    throw  std::invalid_argument("pose_array_topic is not defined");
  }
  array_pub_=nh_.advertise<geometry_msgs::PoseArray>(array_topic,1);
  std::vector<std::string> camera_topics;
  if (!nh_.getParam("camera_topics",camera_topics))
  {
    ROS_ERROR("%s/camera_topics is not defined, aborting",nh_.getNamespace().c_str());
    throw  std::invalid_argument("camera_topics is not defined");
  }
  ROS_DEBUG("%s/Camera topics:",nh_.getNamespace().c_str());
  for (const std::string& str: camera_topics)
    ROS_DEBUG(" - topic: %s",str.c_str());

  std::vector<std::string> camera_frames;
  if (!nh_.getParam("camera_frames",camera_frames))
  {
    ROS_ERROR("%s/camera_frames is not defined, aborting",nh_.getNamespace().c_str());
    throw  std::invalid_argument("camera_frames is not defined");
  }
  ROS_DEBUG("%s/Camera frames:",nh_.getNamespace().c_str());
  for (const std::string& str: camera_frames)
    ROS_DEBUG(" - frame: %s",str.c_str());

  if (camera_frames.size()!=camera_topics.size())
  {
    ROS_ERROR("%s: Dimension mismatch. camera_frames size is %zu, camera_topics size is %zu, aborting",nh_.getNamespace().c_str(),camera_frames.size(),camera_topics.size());
    throw  std::invalid_argument("Dimension mismatch");
  }

  if (!nh_.getParam("robot_reference_frame",robot_reference_frame))
  {
    ROS_ERROR("%s/robot_reference_frame is not defined, aborting",nh_.getNamespace().c_str());
    throw  std::invalid_argument("robot_reference_frame is not defined");
  }
  ROS_DEBUG("%s/robot_reference_frame: %s",nh_.getNamespace().c_str(),robot_reference_frame.c_str());

  if (!nh_.getParam("accuracy_threshold",accuracy_threshold))
  {
    ROS_ERROR("%s/accuracy_threshold is not defined, aborting",nh_.getNamespace().c_str());
    throw  std::invalid_argument("accuracy_threshold is not defined");
  }
  ROS_DEBUG("%s/accuracy_threshold: %f",nh_.getNamespace().c_str(),accuracy_threshold);

  ROS_DEBUG("%s Listening TF...",nh_.getNamespace().c_str());
  tf::TransformListener listener;
  ros::Duration(0.5).sleep(); // waiting for TF;

  for (const std::string& str: camera_frames)
  {
    tf::StampedTransform transform;
    bool found=false;
    for (unsigned int itrial=0;itrial<20;itrial++)
    {
      try
      {
        listener.lookupTransform(robot_reference_frame, str,
                                 ros::Time(0), transform);
        found=true;
        break;
      }
      catch (tf::TransformException ex)
      {
        ros::Duration(0.1).sleep();
      }
    }
    if (!found)
    {
      ROS_ERROR("%s: unable to find TF from %s to %s",nh_.getNamespace().c_str(),robot_reference_frame.c_str(),str.c_str());
      throw  std::invalid_argument("unable to find TF");
    }

    Eigen::Affine3d T_w_cam;
    tf::transformTFToEigen(transform,T_w_cam);
    T_world_cameras.insert(std::pair<std::string,Eigen::Affine3d>(str,T_w_cam));


    std::vector<geometry_msgs::Pose> poses_in_world;
    poses_in_world_frame.insert(std::pair<std::string,std::vector<geometry_msgs::Pose>>(str,poses_in_world));
  }
  for (unsigned int icam=0;icam<camera_frames.size();icam++)
  {
    subPtr sub=std::make_shared<ros_helper::SubscriptionNotifier<shared_human_body_part::BodyList>>(nh,camera_topics.at(icam),1);
    sub->setAdvancedCallback(boost::bind(&PickPlaceWrapper::bodeMsgCb,this,_1,camera_frames.at(icam)));
    subs_.insert(std::pair<std::string,subPtr>(camera_frames.at(icam),sub));
  }
}

void PickPlaceWrapper::bodeMsgCb(const shared_human_body_part::BodyListConstPtr& msg, const std::string& camera_frame)
{
  std::vector<geometry_msgs::Pose> poses_in_camera;
  convertBodyList(msg,poses_in_camera);
  const Eigen::Affine3d& T_w_cam=T_world_cameras.at(camera_frame);
  std::vector<geometry_msgs::Pose>& poses_in_world=poses_in_world_frame.at(camera_frame);
  poses_in_world.resize(poses_in_camera.size());
  for (unsigned int idx=0;idx<poses_in_world.size();idx++)
  {
    Eigen::Affine3d p_in_cam;
    tf::poseMsgToEigen(poses_in_camera.at(idx),p_in_cam);
    Eigen::Affine3d p_in_w=T_w_cam*p_in_cam;
    tf::poseEigenToMsg(p_in_w,poses_in_world.at(idx));
  }
}

void PickPlaceWrapper::convertBodyList(const shared_human_body_part::BodyListConstPtr& msg,std::vector<geometry_msgs::Pose>& poses_in_camera)
{
  poses_in_camera.clear();
  for (const shared_human_body_part::Body b: msg->body_array)
    convertBody(b,poses_in_camera);
}

void PickPlaceWrapper::convertBody(const shared_human_body_part::Body& msg, std::vector<geometry_msgs::Pose> &poses_in_camera)
{
  if (msg.nose.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.nose));
  if (msg.left_eye.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.left_eye));
  if (msg.right_eye.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.right_eye));
  if (msg.left_ear.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.left_ear));
  if (msg.right_ear.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.right_ear));
  if (msg.left_shoulder.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.left_shoulder));
  if (msg.right_shoulder.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.right_shoulder));
  if (msg.left_elbow.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.left_elbow));
  if (msg.right_elbow.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.right_elbow));
  if (msg.left_wrist.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.left_wrist));
  if (msg.right_wrist.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.right_wrist));
  if (msg.left_hip.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.left_hip));
  if (msg.right_hip.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.right_hip));
  if (msg.left_knee.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.left_knee));
  if (msg.right_knee.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.right_knee));
  if (msg.left_ankle.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.left_ankle));
  if (msg.right_ankle.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.right_ankle));
  if (msg.neck.accuracy>=accuracy_threshold)
    poses_in_camera.push_back(convertCoordinate3d(msg.neck));
}

geometry_msgs::Pose PickPlaceWrapper::convertCoordinate3d(const shared_human_body_part::Coordinate3D& p)
{
  geometry_msgs::Pose pose;
  pose.orientation.x=0;
  pose.orientation.y=0;
  pose.orientation.z=0;
  pose.orientation.w=1;
  pose.position.x=p.x;
  pose.position.y=p.y;
  pose.position.z=p.z;
  return pose;
}

void PickPlaceWrapper::publishPoseArray()
{
  geometry_msgs::PoseArray msg;
  msg.header.frame_id=robot_reference_frame;
  msg.header.stamp=ros::Time::now();
  for (const std::pair<std::string,std::vector<geometry_msgs::Pose>>& poses: poses_in_world_frame)
  {
    for (const geometry_msgs::Pose& p: poses.second)
      msg.poses.push_back(p);
  }
  array_pub_.publish(msg);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "pickplace_wrapper");
  ros::NodeHandle nh("~");
  ros::Rate lp(100);
  PickPlaceWrapper wrapper(nh);
  while (ros::ok())
  {
    ros::spinOnce();
    wrapper.publishPoseArray();
    lp.sleep();
  }
  return 0;
}
